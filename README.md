Composer versioning
=================
Basic info: https://getcomposer.org/doc/02-libraries.md#specifying-the-version

If branch has text name (not x.x) for example "bugfixes" can load version as: "salopot/composer-versioning": "dev-bugfixes"
In this reason "main" branch can be loaded as: "salopot/composer-versioning": "dev-master"

If branch has name same as version for example 2.0 can load as (include x): "salopot/composer-versioning": "2.0.x-dev"

For tag: v2.0.0-alpha -> "salopot/composer-versioning": "2.0.0-alpha"
For tag: v2.0.1 -> "salopot/composer-versioning": "2.0.*"


GIT:
Create branch: git checkout -b branch_name
Create tag: git tag v2.0.0-alpha or git tag v2.0.1
Share tag: git push origin v2.0.1


Uses:
    "require": {
        "salopot/composer-versioning": "2.0.*"
    },
    "repositories": [
        {
            "type": "git",
            "url": "https://salopot@bitbucket.org/salopot/composer-versioning.git"
        }
    ]